import React, { Component } from "react";
import { Animated, ActivityIndicator, Alert, Keyboard, StyleSheet, Platform, Text, Image, TextInput, View, TouchableOpacity } from "react-native";
import TrackPlayer from "react-native-track-player"
import { RNS3 } from "react-native-aws3"
import { AudioRecorder, AudioUtils } from "react-native-audio"
import { scale } from 'react-native-size-matters';

export default class LandingScreen extends Component {
  static navigationOptions = {
    headerShown: false
  };
  constructor(props) {
    super(props);

    this.state = {
      opacity: new Animated.Value(0),
      shown: false,
      currentTime: 0.0,
      showContact: false,
      hasPermission: undefined,
      recording: false,
      image: "",
      loading: false,
      finished: false,
      paused: false,
      decibel: 'https://icecast.globallogic.nl/decibel.mp3',
      royaal: 'https://icecast2.globallogic.nl/royaal-n.mp3',
      ibiza: 'https://icecast2.globallogic.nl/soi.mp3',
      submitSuccess: false,
      playingIbiza: false,
      playingRoyaal: false,
      playingDecibel: false,
      buffering: false,
      stoppedRecording: false,
      height: new Animated.Value(80),
      audioPath: AudioUtils.DocumentDirectoryPath + "/test.aac",
      name: "",
      telephone: "",
      message: "",
    };

    TrackPlayer.addEventListener('playback-state', data => {
      this.setState({ buffering: !(data.state === 3 || data.state === 'playing') });
      if(data.state === TrackPlayer.STATE_PLAYING && !(this.state.playingIbiza || this.state.playingRoyaal || this.state.playingDecibel)) {
        TrackPlayer.pause();
      }
      if (data.state !== TrackPlayer.STATE_BUFFERING && (this.state.playingIbiza || this.state.playingRoyaal || this.state.playingDecibel)) {
        TrackPlayer.play()
      }
    })
  }
  cameraScreen = () => this.props.navigation.navigate("Camera")

  async componentDidMount() {
    const response = await fetch('https://sven-jens.nl/decibel/links.json');
    const myJson = await response.json();
    TrackPlayer.setupPlayer().then(async () => {
      await TrackPlayer.add([{
        id: "royaal",
        url: myJson.royaal,
        title: "Radio Royaal",
        artist: "Radio Royaal",
      },
      {
        id: "decibel",
        url: myJson.decibel,
        title: "Radio Decibel",
        artist: "Radio Decibel",
      },
      {
        id: "ibiza",
        url: myJson.ibiza,
        title: "Decibel",
        artist: "Sound of ibiza",
      }
      ]);
    });
    TrackPlayer.updateOptions({
      stopWithApp: true,

      // An array of media controls capabilities
      // Can contain CAPABILITY_PLAY, CAPABILITY_PAUSE, CAPABILITY_STOP, CAPABILITY_SEEK_TO,
      // CAPABILITY_SKIP_TO_NEXT, CAPABILITY_SKIP_TO_PREVIOUS, CAPABILITY_SET_RATING
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP
      ],

      // An array of capabilities that will show up when the notification is in the compact form on Android
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE
      ]
    });

    this.setState({
      decibel: myJson.decibel,
      royaal: myJson.royaal,
      ibiza: myJson.ibiza
    });

    let state = await TrackPlayer.getState();

    if(state === "STATE_PLAYING") {
      this.setState({
        playingDecibel: false,
      })
    }

    await TrackPlayer.add([]);
    AudioRecorder.requestAuthorization().then((isAuthorised) => {
      this.setState({ hasPermission: isAuthorised });

      if (!isAuthorised) return;

      this.prepareRecordingPath(this.state.audioPath);

      AudioRecorder.onProgress = (data) => {
        this.setState({currentTime: Math.floor(data.currentTime)})
      };

      AudioRecorder.onFinished = (data) => {
        // Android callback comes in the form of a promise instead.
        if (Platform.OS === "ios") {
          this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize)
        }
      }
    })
  }

  componentWillUnmount() {
    this.setState({
      playingDecibel: false,
      playingRoyaal: false,
      playingIbiza: false,
    });
    TrackPlayer.pause()
  }

  _record = async () => {
    if (this.state.recording) {
      this._stop();
      return
    }

    if (!this.state.hasPermission) {
      return
    }

    if(this.state.stoppedRecording){
      this.prepareRecordingPath(this.state.audioPath)
    }

    this.setState({recording: true, paused: false});

    try {
      const filePath = await AudioRecorder.startRecording();
    } catch (error) {
      console.error(error)
    }
  };

  _finishRecording = didSucceed => {
    this.setState({
      finished: didSucceed,
    })
  };

  pressRecord = () => {
  if (this.state.recording) {
    this._stop()
  } else {
    this._record()
    }
  };

  prepareRecordingPath = audioPath => {
    AudioRecorder.prepareRecordingAtPath(audioPath, {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "High",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000,
    })
  };

  delay = (ms) => new Promise((res) => setTimeout(res, ms));

  playRadio = async (id, url, title, artist) => {
    this.setState({
      playingDecibel: id === 'decibel',
      playingRoyaal: id === 'royaal',
      playingIbiza: id === 'ibiza',
    });
    // await TrackPlayer.play();

    await TrackPlayer.reset();
    await TrackPlayer.add({
      id,
      url,
      title,
      artist,
    });
    let trackObject = await TrackPlayer.getTrack(id);

    const track = await TrackPlayer.getCurrentTrack();
    const state = await TrackPlayer.getState();
    let tracks = await TrackPlayer.getQueue();
  };

  hideContact = () => {
    this.setState({
      showContact: false,
      submitSuccess: false,
    });
    Animated.timing(                  // Animate over time
      this.state.height,            // The animated value to drive
      {
        toValue: 80,                   // Animate to opacity: 1 (opaque)
        duration: 600,              // Make it take a while
      },
    ).start()                        // Starts the animation
  };

  fadeout = () => {
    Animated.timing(                  // Animate over time
      this.state.opacity,            // The animated value to drive
      {
        toValue: 0,                   // Animate to opacity: 1 (opaque)
        duration: 600,              // Make it take a while
      },
    ).start(() => {
      this.setState({
        shown: false,
      })
    })
  };

  fadein = () => {
    this.setState({
      shown: true,
    });
    Animated.timing(                  // Animate over time
      this.state.opacity,            // The animated value to drive
      {
        toValue: 1,                   // Animate to opacity: 1 (opaque)
        duration: 600,              // Make it take a while
      },
    ).start()                        // Starts the animation
  };

  async _stop() {
    if (!this.state.recording) {
      return
    }

    this.setState({stoppedRecording: true, recording: false, paused: false});

    try {
      const filePath = await AudioRecorder.stopRecording();

      if (Platform.OS === "android") {
        this._finishRecording(true, filePath)
      }
      return filePath
    } catch (error) {
      console.error(error)
    }
  }

  uploadfile = async file => {
    if(!file) return false;
    const options = {
      keyPrefix: "uploads/",
      bucket: "radioroyaal",
      region: "eu-west-3",
      accessKey: "AKIAIMQODAGD7EUHABJQ",
      secretKey: "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",
      successActionStatus: 201,
    };
    return await RNS3.put(file, options).then(response => {
      if (response.status !== 201)
        throw new Error("Failed to upload image to S3")

      return response.body
    }).progress((e) => console.log(e.percent))
  };

  makeid() {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 15; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text
  };

  stopPlaying = () => {
    this.setState({
      playingDecibel: false,
      playingRoyaal: false,
      playingIbiza: false,
    });
    TrackPlayer.pause()
  };

  uploadImage = async () => {
    if(this.props.navigation.state.params.picture) {
      const image = {
        uri: this.props.navigation.state.params.picture,
        name: this.makeid() + ".jpg",
        type: "image/jpg",
      };
      return await this.uploadfile(image)
    }
    return false
  };

  uploadAudio = async () => {
    if(this.state.currentTime !== 0.0) {
      const audio = {
        // `uri` can also be a file system path (i.e. file://)
        uri: this.state.audioPath,
        name: this.makeid() + ".aac",
        type: "audio/acc",
      };
      return await this.uploadfile(audio)
    }
    return false
  };

  submitForm = async () => {
    Keyboard.dismiss();
    const { recording, stoppedRecording } = this.state;
    let responseAudio, response;
    if(!this.state.name || !this.state.telephone || !this.state.message) {
      Alert.alert('Mislukt', 'Je hebt nog alle velden ingevuld');
      return false;
    }
    this.setState({
      loading: true,
    });

    await this._stop();
    if((recording || stoppedRecording) && this.state.currentTime !== 0.0) {
      responseAudio = await this.uploadAudio();
    }
    if(this.props && this.props.navigation && this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.picture) {
      response = await this.uploadImage();
    }
    const body = JSON.stringify(
      {
        name: this.state.name,
        telephone: this.state.telephone,
        message: this.state.message,
        audio: responseAudio && responseAudio.postResponse && responseAudio.postResponse.location || "",
        image: response && response.postResponse && response.postResponse.location || "",
      });
    await fetch("https://sven-jens.nl/decibel/php/submit.php", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
      body,
    }).then(res => {
      this.setState({
        submitSuccess: true,
        loading: false,
        image: "",
        name: "",
        telephone: "",
        currentTime: 0.0,
        message: "",
      });
      setTimeout(() => {
        this.hideContact()
      }, 1500)
    })
      .catch(err => {
        this.setState({
          loading: false,
        });
      })
  };

  pressButton = () => {
    if(this.state.showContact) {
      try {
        this.submitForm();
      }catch (e) {
        console.error(e)
      }
    } else {
      this.setState({
        showContact: true,
      });
      Animated.timing(                  // Animate over time
        this.state.height,            // The animated value to drive
        {
          toValue: 400,                   // Animate to opacity: 1 (opaque)
          duration: 600,              // Make it take a while
        },
      ).start()                        // Starts the animation
    }
  };

  render() {
    const {
      ibiza,
      royaal,
      decibel,
      showContact,
      height,
      name,
      telephone,
      message,
      submitSuccess,
      recording,
      stoppedRecording,
      playingDecibel,
      buffering,
      playingRoyaal,
      playingIbiza,
      loading
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={[styles.row, { backgroundColor: "#1482bc"}]}>
            <Image source={require('./logo.png')} style={styles.logo} />
            { playingDecibel && !buffering &&
              <TouchableOpacity onPress={this.stopPlaying}>
                <Image source={require('./pause.png')} style={styles.playButton}/>
              </TouchableOpacity> }
            { playingDecibel && buffering  &&
                <View styles={{flex: 1 / 3, justifyContent: "flex-end", alignItems: "center"}}>
                  <ActivityIndicator style={styles.playButton} size={'large'} color={'white'}/>
                </View> }
            { !playingDecibel &&
              <TouchableOpacity onPress={() => this.playRadio('decibel', decibel, 'Decibel', 'Radio Decibel')}>
                <Image source={require('./play.png')} style={styles.playButton}/>
              </TouchableOpacity>
            }
        </View>
        <View style={[styles.row, {backgroundColor: "#9d175c"}]}>
          <Image source={require('./royaal.png')} style={styles.logo} />
          { playingRoyaal && !buffering &&
          <TouchableOpacity onPress={this.stopPlaying}>
            <Image source={require('./pause.png')} style={styles.playButton}/>
          </TouchableOpacity> }
          { playingRoyaal && buffering &&
          <View styles={{flex: 1 / 3, justifyContent: "flex-end", alignItems: "center"}}>
            <ActivityIndicator style={styles.playButton} size={'large'} color={'white'}/>
          </View> }
          { !playingRoyaal &&
          <TouchableOpacity onPress={() => this.playRadio('royaal', royaal, 'Radio Royaal', 'Radio Royaal')}>
            <Image source={require('./play.png')} style={styles.playButton}/>
          </TouchableOpacity>
          }
        </View>
        <View style={[styles.row, { backgroundColor: "#044b8d", marginBottom: scale(40)}]}>
          <Image source={require('./ibiza.png')} style={styles.logo} />
          { playingIbiza && !buffering &&
          <TouchableOpacity onPress={this.stopPlaying}>
            <Image source={require('./pause.png')} style={styles.playButton}/>
          </TouchableOpacity> }
          { playingIbiza && buffering &&
          <View styles={{flex: 1 / 3, justifyContent: "flex-end", alignItems: "center"}}>
            <ActivityIndicator style={styles.playButton} size={'large'} color={'white'}/>
          </View> }
          { !playingIbiza &&
          <TouchableOpacity onPress={() => this.playRadio('ibiza', ibiza, 'Radio Decibel', 'Sound of Ibiza')}>
            <Image source={require('./play.png')} style={styles.playButton}/>
          </TouchableOpacity>
          }
        </View>
        <Animated.View style={[styles.footer, { height }]}>
          { showContact && !submitSuccess?
            <View>
              <TouchableOpacity onPress={this.hideContact}><Text style={{ fontWeight: "bold", color: '#fff', textAlign: "right", marginRight: 10}}>SLUITEN</Text></TouchableOpacity>
              <View style={{backgroundColor: "#fff", marginTop: scale(15)}}>
                {loading ? <ActivityIndicator size={"small"}/> : null }
                <TextInput style={{borderTopWidth: 1, paddingHorizontal: 5, borderColor: "#0397d6", height: 40}} value={name} onChangeText={(name) => this.setState({name})} placeholder={"Naam..."} />
                <TextInput style={{borderTopWidth: 1, paddingHorizontal: 5, borderColor: "#0397d6", height: 40}} value={telephone} keyboardType="numeric" onChangeText={(telephone) => this.setState({telephone})} placeholder={"Telefoonnummer..."} />
                <TextInput style={{borderTopWidth: 1, paddingHorizontal: 5, borderColor: "#0397d6",height: "35%", textAlignVertical: "top"}} onSubmitEditing={() => this.submitForm()} returnKeyType={"done"} returnKeyLabel={"Verstuur"} value={message} numberOfLines={6} multiline onChangeText={(message) => this.setState({message})} placeholder={"Laat een bericht achter..."} />
              </View>

              <View style={{flexDirection: "row", justifyContent: "space-around", alignContent: "center", alignItems: "center", paddingTop: scale(20), paddingBottom: scale(20) }}>
                { (recording || stoppedRecording) && this.state.currentTime !== 0.0 ? <Text style={{color: "#fff", fontSize: 22, marginRight: scale(20)}}>{this.state.currentTime}s</Text> : <View style={{marginLeft: scale(20), width: scale(50), height: scale(50), borderRadius: 30}} /> }
                <TouchableOpacity onPress={this.pressRecord} style={{ backgroundColor: "#0397d6",  borderRadius: 30, padding: scale(10), marginRight: scale(20)}}>{ recording ? <Image source={require('./square.png')} style={{tintColor: "#fff"}}/> : <Image source={require('./microphone.png')} style={{tintColor: "#fff"}}/> }</TouchableOpacity>
                <TouchableOpacity onPress={this.cameraScreen} style={{ backgroundColor: "#0397d6", borderRadius: 30, padding: scale(10)}}><Image source={require('./camera.png')} style={{tintColor: "#fff"}}/></TouchableOpacity>
                { this.props && this.props.navigation && this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.picture ? <Image source={{uri: this.props.navigation.state.params.picture}} style={{marginLeft: scale(20), width: scale(50), height: scale(50), borderRadius: 30}}/> : <View style={{marginLeft: scale(20), width: scale(50), height: scale(50), borderRadius: 30}} />}
              </View>
            </View> : null }
          { submitSuccess ?
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity onPress={this.hideContact}><Text style={{ fontWeight: "bold", color: '#ffffff', textAlign: "right", marginRight: scale(10)}}>SLUITEN</Text></TouchableOpacity>
            <View style={{marginVertical: scale(10)}}>
              <Image source={require('./check.png')} style={{tintColor: "#FFF", width: "30%", resizeMode: "contain", alignSelf: "center", justifyContent: "center", alignItems: "center"}} />
            </View>
          </View> : null }
          <TouchableOpacity style={styles.button} onPress={this.pressButton}>
            <Text style={styles.buttonText}>{ showContact ? 'Verstuur je bericht' : 'Stuur een bericht'}</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    width: "100%",
    backgroundColor: "#F5FCFF"
  },
  row: {
    width: '100%',
    flex: 1/3,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  logo: {
    alignSelf: "center",
    marginLeft: scale(40),
    marginRight: scale(30),
    resizeMode: "contain",
    marginTop: scale(25),
    flex: 2/3,
    height: scale(120),
  },
  playButton: {
    tintColor: "#FFF",
    width: scale(120),
    resizeMode: "contain",
    marginTop: scale(100),
    alignSelf: "center",
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1/3
  },
  footer: {
    height: scale(140),
    backgroundColor: "#004183",
    width: "100%",
    zIndex: 1,
    position: 'absolute',
    bottom: 0,
    paddingVertical: scale(15),
  },
  button: {
    paddingVertical: scale(10),
    textAlign: 'center',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: scale(40),
    marginHorizontal: '5%',
    backgroundColor: "#0397d6",
  },
  buttonText: {
    color: '#fff'
  }
});
