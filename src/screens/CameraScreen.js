import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TouchableWithoutFeedback } from 'react-native';
import { RNCamera } from 'react-native-camera';

export default class CameraScreen extends Component {
  state = {
    front: true,
  };

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.4, base64: true }
      const data = await this.camera.takePictureAsync(options)
      this.nextScreen(data.uri)
    }
  };

  nextScreen(picture){
    this.props.navigation.navigate("Landing", {picture: picture})
  }

  handleDoubleTap = () => {
    const now = Date.now()
    const DOUBLE_PRESS_DELAY = 300
    if (this.state.lastTap && (now - this.state.lastTap) < DOUBLE_PRESS_DELAY) {
      this.setState({
        front: !this.state.front,
      })
    } else {
      this.setState({
        lastTap: now,
      })
    }
  };

  render() {
    return(
      <View style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={this.handleDoubleTap} style={{flex: 1, justifyContent: "flex-end", alignItems: "center"}}>
        <RNCamera
          ref={ref => {
            this.camera = ref
          }}
          style={{flex: 1, justifyContent: "flex-end", alignItems: "center"}}
          type={this.state.front ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back }
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={"We need your permission to use your camera phone"}
        />
        </TouchableWithoutFeedback>
        <View style={{ flex: 0, flexDirection: "row", justifyContent: "center" }}>
          <TouchableOpacity onPress={() => this.takePicture()} style={{flex: 0,
            backgroundColor: "#fff",
            borderRadius: 5,
            padding: 15,
            paddingHorizontal: 20,
            alignSelf: "center",
            margin: 20}}>
            <Text style={{ fontSize: 14 }}> SNAP </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
