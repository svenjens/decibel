import React from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';

import LandingScreen from "./src/screens/LandingScreen";
import CameraScreen from "./src/screens/CameraScreen";

import * as Sentry from '@sentry/react-native';

Sentry.init({ 
  dsn: 'https://3bab0cc7cfce4482befb5e186f42ec00@o174068.ingest.sentry.io/5367734', 
});


const AppNavigator = createStackNavigator(
  {
    Landing: {
      screen: LandingScreen
    },
    Camera: {
      screen: CameraScreen
    },
  },
  { initialRouteName: "Landing" }
);

const AppContainer = createAppContainer(AppNavigator);

export default function App() {
  return <AppContainer />;
}
